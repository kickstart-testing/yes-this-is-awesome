Pod::Spec.new do |spec|
    spec.name                     = 'shared'
    spec.version                  = '0.6.1666295807544'
    spec.homepage                 = 'https://touchlab.dev'
    spec.source                   = { 
                                      :http => 'https://gitlab.com/api/v4/projects/40398972/packages/maven/KMMBridgeSampleKotlin/shared-kmmbridge/0.6.1666295807544/shared-kmmbridge-0.6.1666295807544.zip',
                                      :type => 'zip',
                                      :headers => ['Accept: application/octet-stream']
                                    }
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'KMMBridgeSampleKotlin'
    spec.vendored_frameworks      = 'shared.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '13'
            
            
end